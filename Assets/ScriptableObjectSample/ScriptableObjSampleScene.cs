﻿using UnityEngine;

public class ScriptableObjSampleScene : MonoBehaviour
{
    [SerializeField] private ParameterTable _table2;

    public ParameterTable Table2 => _table2;
    // Start is called before the first frame update
    void Start()
    {
    }

    public void GetDataFromScriptableObject()
    {
        Debug.Log($"{ParameterTable.Instance.MaxLife}");
    }

    public void GetdataFromTable2()
    {
        Debug.Log($"{_table2.MaxLife}");
    }

    // Update is called once per frame
    void Update()
    {
    }
}