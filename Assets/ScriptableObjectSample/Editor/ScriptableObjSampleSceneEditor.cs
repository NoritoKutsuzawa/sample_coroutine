﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ScriptableObjSampleScene))]
public class ScriptableObjSampleSceneEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (GUILayout.Button("Button1"))
        {
            Debug.Log("押した!");
            var parameter = target as ScriptableObjSampleScene;

            parameter?.GetDataFromScriptableObject();
        }

        if (GUILayout.Button("Button2"))
        {
            var parameter = target as ScriptableObjSampleScene;

            parameter?.GetdataFromTable2();
        }
    }
}