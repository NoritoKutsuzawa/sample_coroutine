﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "MyGame/Create Parameter Table", fileName = "NewTable")]
public class ParameterTable : ScriptableObject
{
    private const string RESOURCES_PATH = "NewTable";
    private static ParameterTable _instance;
    [SerializeField] private List<List<int>> GrowthValueList;
    [SerializeField] private List<int> GrowthValues;
    
    public int MaxLife => maxLife;
    [SerializeField] private int maxLife;

    public static ParameterTable Instance
    {
        get
        {
            if (_instance == null)
            {
                var asset = Resources.Load(RESOURCES_PATH) as ParameterTable;
                if (asset == null)
                {
                    asset = CreateInstance<ParameterTable>();
                }

                _instance = asset;
            }

            return _instance;
        }
    }

}