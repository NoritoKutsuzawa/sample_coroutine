﻿using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;

public class CombinationTest
{
    [OneTimeSetUp]
    public void InitializeTableData()
    {
        _tableDatas = new List<int>
        {
            1, 2, 4, 6, 9, 12, 16
        };

        _tableCount = _tableDatas.Count;
    }

    private List<int> _tableDatas;
    private static int _tableCount = 7;

    //[TestCaseSource(nameof(TestCombinationTestCases))]
    public void CombinationCheckTest(List<int> indexCombinations)
    {
        var result = 0;

        foreach (var index in indexCombinations)
        {
            result += _tableDatas[index];
        }

        if (result == 40)
            Debug.Log(
                $"{indexCombinations[0]}:" +
                $"{indexCombinations[1]}:" +
                $"{indexCombinations[2]}:" +
                $"{indexCombinations[3]}:" +
                $"{indexCombinations[4]}" +
                $" => {result}");
    }

    private static IEnumerable<TestCaseData> TestCombinationTestCases
    {
        get
        {
            for (var i = 0; i < _tableCount; i++)
            {
                for (var j = 0; j < _tableCount; j++)
                {
                    for (var k = 0; k < _tableCount; k++)
                    {
                        for (var l = 0; l < _tableCount; l++)
                        {
                            for (var m = 0; m < _tableCount; m++)
                            {
                                yield return new TestCaseData(new List<int> {i, j, k, l, m});
                            }
                        }
                    }
                }
            }
        }
    }

    [Test]
    public void CombinationCheck()
    {
        var totalCount = 0;
        for (var i = 0; i < 7; i++)
        for (var j = 0; j < 7; j++)
        for (var k = 0; k < 7; k++)
        for (var l = 0; l < 7; l++)
        for (var m = 0; m < 7; m++)
        {
            var result = CalcTotal(i, j, k, l, m);
            if (result == 40)
            {
                totalCount++;
                Debug.Log(
                    $"{totalCount}::" +
                    $"{i}:" +
                    $"{j}:" +
                    $"{k}:" +
                    $"{l}:" +
                    $"{m}" +
                    $"=>" +
                    $"{_tableDatas[i]}:" +
                    $"{_tableDatas[j]}:" +
                    $"{_tableDatas[k]}:" +
                    $"{_tableDatas[l]}:" +
                    $"{_tableDatas[m]}" +
                    $" => {result}");
            }
        }

        Debug.Log($"Total :{totalCount}");
    }

    private int CalcTotal(int i, int j, int k, int l, int m)
    {
        var result = 0;
        result += _tableDatas[i];
        result += _tableDatas[j];
        result += _tableDatas[k];
        result += _tableDatas[l];
        result += _tableDatas[m];
        return result;
    }
}