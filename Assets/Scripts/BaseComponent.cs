﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class BaseComponent : MonoBehaviour
{
	private float number;

	public void Init(float num)
	{
		number = num;
	}

	// Start is called before the first frame update
	void Start()
	{
	}

	public IEnumerator BaseCoroutine()
	{
		yield return new WaitForSeconds(number);
		Debug.Log($"{number} =>{DateTime.Now}");

		//yield return null;
	}

	public IEnumerator BaseCoroutine(float waitSecond, UnityAction callBack)
	{
		yield return new WaitForSeconds(waitSecond);
		Debug.Log($"R:{number} =>{DateTime.Now}");
		callBack.Invoke();
		//yield return null;
	}

	// Update is called once per frame
	void Update()
	{
	}
}