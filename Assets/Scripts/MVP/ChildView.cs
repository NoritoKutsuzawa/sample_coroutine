﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ChildView : MonoBehaviour
{
	public Button IncreaseButton
	{
		get => increaseButton;
		set => increaseButton = value;
	}

	[SerializeField] private Button increaseButton;


	public UnityAction OnIncreaseButtonClickAction;


	private void Awake()
	{
	}

	private void Start()
	{
		Init();
	}

	private void Init()
	{
		Debug.Log($"Init : view");
		increaseButton.onClick.AddListener(OnIncreaseButtonClickAction);
	}


	public void DisplayValue(int value)
	{
		Debug.Log($"Changed!{value}");
	}

	// Update is called once per frame
	void Update()
	{
	}
}