﻿using UnityEngine;
using UnityEngine.Events;

public class ChildModel
{
	public int ExpProperty
	{
		get { return expProperty; }
		set
		{
			if (expProperty != value)
			{
				OnExpPropertyChanged?.Invoke(expProperty);
				Debug.Log($"Invoked => {expProperty}:{value}");
			}
			else
				Debug.Log($"Not Invoked => {expProperty}:{value}");

			expProperty = value;
		}
	}

	private int expProperty;

	public UnityAction<int> OnExpPropertyChanged;


	public void Init()
	{
		//Modelの初期化を行う

		//スタート時にうまく動かないときがあるので、OnExpPropertyChangeを動かす流れを作りたい

		//実際に動かしてしまうか
		//OnExpPropertyChanged?.Invoke(expProperty);

		//明示的に初期値0で動かす流れを作るか
		//おそらくこちらの方が良い
		ExpProperty = 0;
	}
}