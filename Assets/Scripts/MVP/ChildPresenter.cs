﻿using UnityEngine;

public class ChildPresenter : MonoBehaviour
{
	[SerializeField] private ChildView viewObject;

	private ChildModel modelObject = new ChildModel();

	private void Awake()
	{
		Init();
	}

	private void Init()
	{
		Debug.Log($"Init : presenter");

		//UIがもつEventに登録するよりも、UnityAction変数を用意して代入した方がより疎結合になる
		//例えば、Button.OnClickからToggle.OnValueChangeに変更になったときに、
		//後者の場合、Viewの変更だけで済むが
		//前者の場合、ViewとPresenterの変更が必要になる

		viewObject.OnIncreaseButtonClickAction += () => { modelObject.ExpProperty++; };
		//viewObject.IncreaseButton.onClick.AddListener( () => { modelObject.ExpProperty++; });

		modelObject.OnExpPropertyChanged += (value) => viewObject.DisplayValue(value);

		modelObject.Init();
	}

	// Start is called before the first frame update
	void Start()
	{
	}

	// Update is called once per frame
	void Update()
	{
	}
}