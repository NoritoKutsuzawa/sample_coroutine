﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class LongPressButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
	[Header("長押しと判定するまでの時間")] [SerializeField]
	private float longPressInterval;

	[SerializeField] private float nextTime;

	public UnityEvent PointerDownEvent { get; set; } = new UnityEvent();

	public UnityEvent PointerUpEvent { get; set; } = new UnityEvent();

	/// <summary>押されている状態かどうか</summary>
	private bool _beingPressed;

	/// <summary>長押しと判定されたかどうか</summary>
	private bool _isLongPressed;

	public bool IsButtonLongPressed
	{
		get => _isLongPressed;
		set
		{
			if (value != _isLongPressed)
				OnIsLongPressedChangedAction.Invoke(value);

			_isLongPressed = value;
		}
	}

	/// <summary>長押しされたときに一度だけ呼ばれる</summary>
	public UnityAction<bool> OnIsLongPressedChangedAction;

	/// <summary>通常クリックされた時のアクション</summary>
	public UnityAction OnNormalClickAction { get; set; }

	public void OnPointerDown(PointerEventData eventData)
	{
		//登録されたEventを実行する
		PointerDownEvent?.Invoke();

		//押している状態フラグをOnにする
		_beingPressed = true;

		//押したときにここを超えたら長押し判定となる時刻（秒）を設定する
		nextTime = Time.realtimeSinceStartup + longPressInterval;
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		//長押し判定されていない時は、通常のClickEventが呼ばれる
		if (!IsButtonLongPressed)
			OnNormalClickAction?.Invoke();

		//登録されたPointerUpEventを実行する
		PointerUpEvent?.Invoke();

		//押している状態フラグをOffにする
		_beingPressed = false;

		//長押しフラグをOffにする
		IsButtonLongPressed = false;
	}

	private IEnumerator JudgeLongpressCoroutine()
	{
		while (true)
		{
			if (_beingPressed && nextTime < Time.realtimeSinceStartup)
				IsButtonLongPressed = true;

			yield return null;
		}
	}

	private void Start()
	{
		StartCoroutine(JudgeLongpressCoroutine());
	}
}