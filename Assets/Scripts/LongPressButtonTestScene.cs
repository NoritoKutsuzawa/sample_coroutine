﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class LongPressButtonTestScene : MonoBehaviour
{
	[SerializeField] private Button _longPressButton;

	[SerializeField] private LongPressButton _longPressButton2;
	[SerializeField] private UnityEvent _pointerDownEvent;

	[SerializeField] private UnityEvent _pointerUpEvent;

	[SerializeField] private int DisplayCount;
	private bool startTick;
	private IEnumerator SwitchTimerCoroutine;

	/// <summary>Tickする時間間隔 </summary>
	[SerializeField] private float tickInterval;

	public UnityEvent PointerDownEvent
	{
		get => _pointerDownEvent;
		set => _pointerDownEvent = value;
	}

	void Start()
	{
		_longPressButton.onClick.AddListener(ImageCopyTest);
		_pointerDownEvent.AddListener(() => Debug.Log("↓ Click"));
		_pointerUpEvent.AddListener(() => Debug.Log("↑ Click"));

		_longPressButton2.PointerDownEvent.AddListener(() => Debug.Log("LONG ↓ Click"));
		_longPressButton2.PointerUpEvent.AddListener(() => Debug.Log("LONG ↑ Click"));
		_longPressButton2.OnIsLongPressedChangedAction += SwitchCoroutineTimer;
		_longPressButton2.OnNormalClickAction += () => Debug.Log($"Normal Button Yeaah");

		SwitchTimerCoroutine = TickTimerCoroutine();
	}

	[SerializeField] private Sprite _nextSprite;

	public void ImageCopyTest()
	{
		var originImage = _longPressButton.gameObject.GetComponent<Image>().sprite;

		//var targetImage = _longPressButton2.gameObject.GetComponent<Image>().sprite;
		//targetImage = originImage;
		_longPressButton2.gameObject.GetComponent<Image>().sprite = originImage;

		originImage = _nextSprite;

		_longPressButton.gameObject.GetComponent<Image>().sprite = originImage;


		DestroyImmediate(_longPressButton.gameObject);
	}

	void Update()
	{
	}

	public void SwitchCoroutineTimer(bool isLongPress)
	{
		//反転させる
		//Start時にfalseでアルという前提
		startTick = isLongPress;

		if (startTick)
		{
			//Start
			//coroutineを再度取得する
			SwitchTimerCoroutine = TickTimerCoroutine();
			StartCoroutine(SwitchTimerCoroutine);
			Debug.Log("Start");
		}
		else
		{
			//Stop
			StopCoroutine(SwitchTimerCoroutine);
			Debug.Log("Stop");
		}
	}

	public void OnPointerDown()
	{
		_pointerDownEvent.Invoke();
	}

	public void OnPointerUp()
	{
		_pointerUpEvent.Invoke();
	}

	public IEnumerator TickTimerCoroutine()
	{
		while (true)
		{
			yield return new WaitForSeconds(tickInterval);
			DisplayCount++;
			Debug.Log($"Tick {DisplayCount}");
		}
	}
}

public class IntReactive
{
	private int _value;
	public UnityAction<int> OnChanged;

	public int Value
	{
		get => _value;
		set
		{
			if (value != _value)
				OnChanged?.Invoke(value);

			_value = value;
		}
	}
}

public class BoolReactive
{
	private bool _value;
	public UnityAction<bool> OnChanged;

	public bool Value
	{
		get => _value;
		set
		{
			if (value != _value)
				OnChanged?.Invoke(value);

			_value = value;
		}
	}
}

public class NonMonoModel
{
	public Button MonoButton;

	public IEnumerator NonMonoCoroutine()
	{
		yield return null;
	}

	//public void TestCoroutine()
	//{
	//    StartCoroutine(NonMonoCoroutine());
	//}
}