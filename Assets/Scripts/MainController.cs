﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

public class MainController : MonoBehaviour
{
	[SerializeField] private BaseComponent componentGameObject;
	[SerializeField] private List<BaseComponent> components = new List<BaseComponent>();
	[SerializeField] private int componentCount;

	private void Awake()
	{
		for (var i = 0; i < componentCount; i++)
		{
			var item = Instantiate<BaseComponent>(componentGameObject);
			item.Init((i + 1) * 0.4f);
			components.Add(item);
		}
	}

	// Start is called before the first frame update
	void Start()
	{
		//StartCoroutine(baseCoroutine());
		//StartCoroutine(ListCoroutine());
		StartCoroutine(ListCoroutineRandom());
	}

	private IEnumerator baseCoroutine()
	{
		yield return coroutine1();
		yield return coroutine2();
		yield return coroutine3();
	}

	private IEnumerator coroutine1()
	{
		Debug.Log("1");
		yield return null;
	}

	private IEnumerator coroutine2()
	{
		Debug.Log("2");
		yield return null;
	}

	private IEnumerator coroutine3()
	{
		Debug.Log($"Now => A :{DateTime.Now}");
		yield return new WaitForSeconds(5);
		Debug.Log("3");
		Debug.Log($"Now => b :{DateTime.Now}");
		yield return null;
	}

	//----------リストで順にCoroutineを実行する
	[SerializeField] private bool isFlag;

	private IEnumerator ListCoroutine()
	{
		if (isFlag)
		{
			Debug.Log("Start 1!!");
			yield return ListCoroutine1();
		}
		else
		{
			Debug.Log("Start 2!!");
			yield return ListCoroutine2();
		}

		Debug.Log("DONE");
		yield return null;
	}

	private IEnumerator ListCoroutine1()
	{
		Debug.Log("Start 1");
		foreach (var item in components)
		{
			yield return StartCoroutine(item.BaseCoroutine());
		}

		Debug.Log("END");
		yield return null;
	}

	private IEnumerator ListCoroutine2()
	{
		Debug.Log("Start 2");
		yield return StartCoroutine(components[0].BaseCoroutine());
		yield return StartCoroutine(components[1].BaseCoroutine());
		yield return StartCoroutine(components[2].BaseCoroutine());
		yield return StartCoroutine(components[3].BaseCoroutine());
		yield return StartCoroutine(components[4].BaseCoroutine());

		Debug.Log("END");
		yield return null;
	}

	/// <summary>
	/// Coroutine処理を並列実行し、全ての処理の完了を待つ
	/// </summary>
	/// <returns></returns>
	private IEnumerator ListCoroutineRandom()
	{
		var i = 0;
		UnityAction callBack = () => { i++; };
		Debug.Log("Random Start");
		foreach (var item in components)
		{
			//yield return 
			StartCoroutine(item.BaseCoroutine((float) Random.Range(1, 5), callBack));
		}

		yield return new WaitUntil(() => i == components.Count);
		Debug.Log("END");
		yield return null;
	}

	// Update is called once per frame
	void Update()
	{
	}
}