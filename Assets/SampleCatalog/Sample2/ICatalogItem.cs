﻿using UnityEngine;
using System.Collections;

namespace sample2
{
    public interface ICatalogItem
    {
	    void Initialize(ICatalogItemData itemData);
		void Apply();
    }
}