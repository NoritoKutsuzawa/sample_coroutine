﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sample2
{

    public class CommonCatalog : MonoBehaviour
    {
        public ICatalogItem CatalogItemInterface;
        public CatalogItemBase<ExCatalogItemData> CatalogItem;

        public void DisplayCatalogItem(ICatalogItemData itemData)
        {
            //CatalogItem.Initialize(itemData);
        }

        public void DisplayCatalogItem(List<ICatalogItemData> itemDatas)
        {
            DisplayCatalogItem(itemDatas[0]);
        }

    }

}