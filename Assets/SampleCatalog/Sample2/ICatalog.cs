﻿using System.Collections.Generic;
using UnityEngine.Events;

namespace sample2
{
	public interface ICatalog
	{
		void Initialize(List<ICatalogItemData> itemDatas,UnityAction<ICatalogItemData> onItemClickAction);
		void DisplayCatalogItem();
	}
}