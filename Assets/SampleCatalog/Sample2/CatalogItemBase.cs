﻿using UnityEngine;
using System.Collections;

namespace sample2
{
    public class CatalogItemBase<T> : MonoBehaviour, ICatalogItem where T : ICatalogItemData
    {
        protected virtual T CatalogItemData { get; set; }
        public void Initialize(ICatalogItemData itemData)
        {
			Debug.Log("Initialize!!");
            CatalogItemData = (T)itemData;
        }

        public virtual void Apply()
        {

        }

    }

}