﻿using UnityEngine;

namespace sample2
{
	public class CharacterCatalogItemData : ICatalogItemData
	{
		public int CharacterId { get; set; }
	}
}