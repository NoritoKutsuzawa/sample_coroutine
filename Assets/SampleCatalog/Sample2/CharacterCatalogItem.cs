﻿using System.IO.Ports;
using UnityEngine;
using UnityEngine.UI;

namespace sample2
{
	public class CharacterCatalogItem : CatalogItemBase<CharacterCatalogItemData>
	{
		[SerializeField] private Text _text;

		public override void Apply()
		{
			_text.text = CatalogItemData.CharacterId.ToString();
		}

	}
}