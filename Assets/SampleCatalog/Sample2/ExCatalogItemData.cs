﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sample2
{
    public class ExCatalogItemData : ICatalogItemData
    {
        public int Id { get; set; }
    }
}