﻿using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Events;

namespace sample2
{
	public class CatalogBase<TItemData> : MonoBehaviour, ICatalog
		where TItemData : ICatalogItemData
	{
		[SerializeField] public CatalogItemBase<TItemData> CatalogItem;

		public UnityAction<TItemData> OnItemClickAction { get; set; }

		public List<TItemData> CatalogItemDatas { get; set; }=new List<TItemData>();

		public virtual void DisplayCatalogItem()
		{
			foreach (var itemData in CatalogItemDatas)
			{
				var item = Instantiate(CatalogItem);
				item.Initialize(itemData);
			}
		}

		public void Initialize(List<ICatalogItemData> itemDatas, [CanBeNull] UnityAction<ICatalogItemData> onItemClickAction)
		{
			CatalogItemDatas.Clear();
			foreach (var itemData in itemDatas)
				CatalogItemDatas.Add((TItemData)itemData);

			OnItemClickAction += data =>{ onItemClickAction?.Invoke(data);};
		}
	}
}