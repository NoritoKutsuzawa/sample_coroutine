﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace sample2
{
    /// <summary>
    /// Catalogに配置するItem
    /// </summary>
    public class ExCatalogItem : CatalogItemBase<ExCatalogItemData>
    {
        public Text Text2;

        public override void Apply()
        {
            base.Apply();
            Text2.text = CatalogItemData.Id.ToString();
        }

    }
}
