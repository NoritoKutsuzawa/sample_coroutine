﻿using UnityEngine;

namespace sample2
{
	public class SceneController : MonoBehaviour
	{
		//Cannot Attach
		public sample1.CatalogItemBase<sample1.ExCatalogItemData> CatalogItem1;

		//Cannot Attach
		public CatalogItemBase<ExCatalogItemData> CatalogItem2;

		[SerializeField] private ExCatalogItem _exCatalogItem;
		[SerializeField] private CharacterCatalogItem _characterCatalogItem;

		private ICatalogItem _catalogItem;
		// Start is called before the first frame update
		void Start()
		{
			Debug.Log("Start!!");

			var data = new ExCatalogItemData {Id = 100};
			_catalogItem = _exCatalogItem;
			_catalogItem.Initialize(data);
			_catalogItem.Apply();

			var data2 = new CharacterCatalogItemData {CharacterId = 999};
			_catalogItem = _characterCatalogItem;
			_catalogItem.Initialize(data2);
			_catalogItem.Apply();
		}

		// Update is called once per frame
		void Update()
		{
		}
	}
}