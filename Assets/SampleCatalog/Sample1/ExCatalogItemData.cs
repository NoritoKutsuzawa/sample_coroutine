﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sample1
{
    public class ExCatalogItemData : ICatalogItemData
    {
        public int Id { get; set; }
    }
}