﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace sample1
{
    public interface ICatalog<TItem, TItemData> where TItem : ICatalogItem where TItemData : ICatalogItemData
    {
        TItem CatalogItem { get; set; }

        void Initialize(TItemData itemData);

        void Initialize(List<TItemData> itemDatas);

    }
}