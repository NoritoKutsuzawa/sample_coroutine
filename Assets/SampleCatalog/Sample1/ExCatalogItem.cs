﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace sample1
{
    /// <summary>
    /// Catalogに配置するItem
    /// </summary>
    public class ExCatalogItem : CatalogItemBase<ExCatalogItemData>
    {
        public Text Text;

        protected override ExCatalogItemData CatalogItemData { get; set; }

        public override void Apply()
        {
            Text.text = CatalogItemData.Id.ToString();
        }

        //public override void Initialize(ExCatalogItemData itemData)
        //{

        //}

    }
}
