﻿using UnityEngine;
using System.Collections;

namespace sample1
{
    public interface ICatalogItem/*<T> where T : ICatalogItemData*/
    {
        //void Initialize(T itemData);

        void Apply();

    }
}