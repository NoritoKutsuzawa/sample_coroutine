﻿using UnityEngine;
using System.Collections;

namespace sample1
{
    public abstract class CatalogItemBase<T> : MonoBehaviour, ICatalogItem where T : ICatalogItemData
    {
        protected abstract T CatalogItemData { get; set; }
        public virtual void Initialize(T itemData)
        {
            CatalogItemData = itemData;
            Apply();
        }

        public virtual void Hoge(T itemData)
        {

        }

        public abstract void Apply();
    }

}