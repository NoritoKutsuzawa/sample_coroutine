﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace sample1
{
    /// <summary>
    /// Catalog本体
    /// </summary>
    public class ExCatalog : MonoBehaviour, ICatalog<ExCatalogItem, ExCatalogItemData>
    {
        /// <summary>
        /// Prefab
        /// </summary>
        public ExCatalogItem CatalogItem { get; set; }


        public void DisplayCatalogItem(ExCatalogItemData itemData)
        {
            CatalogItem.Initialize(itemData);
        }

        public void Initialize(ExCatalogItemData itemData)
        {
            throw new System.NotImplementedException();
        }

        public void Initialize(List<ExCatalogItemData> itemDatas)
        {
            Initialize(itemDatas[0]);
        }

        public Func<int, string> SampleFunction { get; set; }

        public void Start()
        {
            SampleFunction += (num) => { return num.ToString(); };
            SampleFunction += DelegateFunction;
        }

        private string DelegateFunction(int num)
        {
            return num.ToString();
        }

        public void ExecuteFunction()
        {
            var str = SampleFunction(1);
            SampleFunction(1);
        }
    }

}
