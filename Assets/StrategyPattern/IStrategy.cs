﻿using System.Collections;
using UnityEngine.Events;

namespace SampleStrategy
{
    public interface IStrategy
    {
	    string className { get; set; }
        int ExecuteCount { get; set; }
        UnityAction<string> OnClickAction { get; set; }
        IEnumerator StartApi();
        void DisplayData();
    }
}