﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace SampleStrategy
{
    public class CharacterStrategy : IStrategy
    {
	    public CharacterStrategy()
	    {
		    OnClickAction = OutputLog;
	    }
	    private void OutputLog(string str)
	    {
		    Debug.Log(className);
	    }
		public string className { get; set; } = "chara";
		public UnityAction<string> OnClickAction { get; set; }

		public int ExecuteCount { get; set; }

        public IEnumerator StartApi()
        {
            Debug.Log("StartApi:start:Chara");

            yield return new WaitForSeconds(1f);
            Debug.Log("StartApi:end:Chara");
        }

        public void DisplayData()
        {
            Debug.Log($"DisplayData:Chara{++ExecuteCount}");
        }
    }
}