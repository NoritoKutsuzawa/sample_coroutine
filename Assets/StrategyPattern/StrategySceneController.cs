﻿using System;
using System.Collections;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

namespace SampleStrategy
{
	public class StrategyFactory
	{
		private IStrategy _characterStrategy;
		private IStrategy _itemStrategy;
		private IStrategy _presentStrategy;

		public IStrategy Create(StrategySceneController.CatalogMode mode)
		{
			switch (mode)
			{
				case StrategySceneController.CatalogMode.Character:
					//return _characterStrategy ?? (_characterStrategy = new CharacterStrategy());
					return _characterStrategy ?? (_characterStrategy = new CharaGenericStrategy(CharaDelegete));
				case StrategySceneController.CatalogMode.Item:
					return _itemStrategy ?? (_itemStrategy = new ItemStrategy());
				case StrategySceneController.CatalogMode.Present:
					return _presentStrategy ?? (_presentStrategy = new PresentStrategy());
				default:
					throw new Exception();
			}
		}

		private void CharaDelegete(CharaItemData data)
		{
			Debug.Log("hoge chara");
		}
	}

	public class StrategySceneController : MonoBehaviour
	{
		public enum CatalogMode
		{
			None,

			/// <summary> キャラ </summary>
			Character,

			/// <summary> アイテム </summary>
			Item,

			/// <summary> プレゼント </summary>
			Present
		}

		public CatalogMode SceneCatalogMode
		{
			get => _sceneCatalogMode;
			set
			{
				if (_sceneCatalogMode != value)
					OnSceneCatalogModeChangedAction.Invoke(value);

				_sceneCatalogMode = value;
			}
		}

		private CatalogMode _sceneCatalogMode = CatalogMode.None;
		private UnityAction<CatalogMode> OnSceneCatalogModeChangedAction;

		private void Start()
		{
			Debug.Log($"Ready!");
			OnSceneCatalogModeChangedAction += StartStrategy;
		}

		private void StartStrategy(CatalogMode mode)
		{
			var strategy = _strategyFactory.Create(mode);
			if (mode == CatalogMode.Character)
			{
				var data =  strategy as CharaGenericStrategy;

				CheckStrategyAction = (str) => { data.GenericAction.Invoke(null);};
			}
			else
			{
				CheckStrategyAction = strategy.OnClickAction;
			}

			StartCoroutine(StartStrategy(_strategyFactory.Create(mode)));
		}

		public void StartItemStrategy()
		{
			SceneCatalogMode = CatalogMode.Item;
		}

		public void StartCharacterStrategy()
		{
			SceneCatalogMode = CatalogMode.Character;
		}

		private IEnumerator StartStrategy(IStrategy strategy)
		{
			yield return strategy.StartApi();

			strategy.DisplayData();
		}

		public UnityAction<string> CheckStrategyAction { get; set; }

		public void CheckStrategy()
		{
			CheckStrategyAction.Invoke("");
		}

		#region Factory

		private readonly StrategyFactory _strategyFactory = new StrategyFactory();

		#endregion

		private void Update()
		{
		}
	}

	[CustomEditor(typeof(StrategySceneController))]
	public class StrategySceneControllerEditor : Editor
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();
			//targetを変換して対象を取得
			var exampleScript = target as StrategySceneController;

			//PublicMethodを実行する用のボタン
			if (GUILayout.Button("Chara"))
				exampleScript.StartCharacterStrategy();
			if (GUILayout.Button("Item"))
				exampleScript.StartItemStrategy();
			if (GUILayout.Button("Present"))
				exampleScript.SceneCatalogMode = StrategySceneController.CatalogMode.Present;
			if (GUILayout.Button("Check"))
				exampleScript.CheckStrategy();
		}
	}
}