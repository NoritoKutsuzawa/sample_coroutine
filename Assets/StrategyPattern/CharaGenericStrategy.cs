﻿using System.Collections;
using UnityEngine.Events;

namespace SampleStrategy
{
	public class CharaGenericStrategy : IStrategyGeneric<CharaItemData>
	{
		public CharaGenericStrategy(UnityAction<CharaItemData> delegateAction)
		{
			GenericAction = delegateAction;
		}
		public string className { get; set; }
		public int ExecuteCount { get; set; }
		public UnityAction<string> OnClickAction { get; set; }

		public IEnumerator StartApi()
		{
			yield break;
		}

		public void DisplayData()
		{
		}

		public UnityAction<CharaItemData> GenericAction { get; set; }
	}
}