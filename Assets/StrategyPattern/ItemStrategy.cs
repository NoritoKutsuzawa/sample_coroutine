﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace SampleStrategy
{
    public class ItemStrategy : IStrategy
    {
	    public ItemStrategy()
	    {
		    OnClickAction = OutputLog;
	    }

	    private void OutputLog(string str)
	    {
			Debug.Log(className);
	    }
	    public string className { get; set; } = "item";
	    public UnityAction<string> OnClickAction { get; set; }

        public int ExecuteCount { get; set; }

        public IEnumerator StartApi()
        {
            Debug.Log("StartApi:start:ITEM");

            yield return new WaitForSeconds(0.5f);
            Debug.Log("StartApi:waiting...");

            yield return new WaitForSeconds(0.2f);
            Debug.Log("StartApi:end:ITEM");
        }

        public void DisplayData()
        {
            Debug.Log($"DisplayData:ITEM{++ExecuteCount}");
        }
    }
}