﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace SampleStrategy
{
    public class PresentStrategy : IStrategy
    {
	    public PresentStrategy()
	    {
		    OnClickAction = OutputLog;
	    }

	    private void OutputLog(string str)
	    {
		    Debug.Log(className);
	    }
		public string className { get; set; } = "present";

		public UnityAction<string> OnClickAction { get; set; }

		public int ExecuteCount { get; set; }

        public IEnumerator StartApi()
        {
            Debug.Log("StartApi:start:Present");

            yield return new WaitForSeconds(0.1f);
            Debug.Log("StartApi:waiting...");

            yield return new WaitForSeconds(0.5f);
            Debug.Log("StartApi:waiting...");

            yield return new WaitForSeconds(0.2f);
            Debug.Log("StartApi:end:Present");
        }

        public void DisplayData()
        {
            Debug.Log($"DisplayData:Present{++ExecuteCount}");
        }
    }
}