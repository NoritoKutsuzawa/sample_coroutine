﻿using UnityEngine.Events;

namespace SampleStrategy
{
	public interface IStrategyGeneric<T> : IStrategy where T : IStrategyData
	{
		UnityAction<T> GenericAction { get; set; }
	}
}