﻿namespace CompareParameter
{
    public class ModelClass
    {
        public Parameter Param { get; set; } = new Parameter();

        public ParamStruct ParamStruct { get; set; }
    }
}