﻿namespace CompareParameter
{
    public class Parameter
    {
        public Parameter()
        {
        }

        public Parameter(Parameter param)
        {
            Id = param.Id;
            Name = param.Name;
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public Parameter Clone()
        {
            return (Parameter) MemberwiseClone();
        }

        public bool Compare(Parameter targetParam)
        {
            if (targetParam.Id != Id)
                return false;
            if (targetParam.Name != Name)
                return false;

            return true;
        }
    }

    public struct ParamStruct
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ParamStruct(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public ParamStruct(ParamStruct param)
        {
            Id = param.Id;
            Name = param.Name;
        }
    }
}