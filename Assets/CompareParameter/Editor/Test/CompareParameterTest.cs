﻿using CompareParameter;
using NUnit.Framework;
using UnityEngine;

public class CompareParameterTest :TestBase
{
    public Parameter param { get; set; }

    [SetUp]
    public void Init()
    {
        Debug.Log("Setup");
        param = new Parameter { Id = 1, Name = "Hoge" };
        model = new ModelClass { Param = param };
        view = new ViewClass();
    }


    [Test]
    public void CompareIntTest1()
    {
        //インスタンスを渡している
        view.Param = model.Param;

        view.Param.Id = 2;

        Assert.AreEqual(view.Param.Id, model.Param.Id);
    }

    [Test]
    public void CompareIntTest2()
    {
        var newParam = new Parameter(model.Param);

        view.Param = newParam;
        view.Param.Id = 2;

        Assert.AreNotEqual(view.Param.Id, model.Param.Id);
    }

    [Test]
    public void CompareStringTest1()
    {
        //インスタンスを渡している
        view.Param = model.Param;

        view.Param.Name = "HHHH";
        Assert.AreEqual(view.Param.Name, model.Param.Name);
    }

    [Test]
    public void CompareStringTest2()
    {
        var newParam = new Parameter(model.Param);
        view.Param = newParam;

        view.Param.Name = "HHHH";
        Assert.AreNotEqual(view.Param.Name, model.Param.Name);
    }

    [Test]
    public void ComparewithShallowCopyTest1()
    {
        //インスタンスを渡している
        view.Param = model.Param;

        view.Param.Id = 2;
        view.Param.Name = "HHHH";

        Assert.AreEqual(view.Param.Id, model.Param.Id);
        Assert.AreEqual(view.Param.Name, model.Param.Name);
    }

    [Test]
    public void ComparewithShallowCopyTest2()
    {
        var newParam = model.Param.Clone();
        view.Param = newParam;

        view.Param.Id = 2;
        view.Param.Name = "HHHH";

        Assert.AreNotEqual(view.Param.Id, model.Param.Id);
        Assert.AreNotEqual(view.Param.Name, model.Param.Name);
    }


}
public class CompareStructParamTest :TestBase
{
    public ParamStruct param { get; set; }

    [SetUp]
    public void Init()
    {
        Debug.Log("Setup s");

        param = new ParamStruct(_defaultId, _defaultName);
        model = new ModelClass { ParamStruct = param };
        view = new ViewClass();
    }

    [Test]
    public void CompareIntTest1()
    {
        //インスタンスを渡している
        view.ParamStruct = model.ParamStruct;

        view.ParamStruct = new ParamStruct(2,_defaultName);

        Assert.AreEqual(view.ParamStruct.Id, model.ParamStruct.Id);
    }

    [Test]
    public void CompareIntTest2()
    {
        var newParam = new ParamStruct(model.ParamStruct);

        view.ParamStruct = newParam;
        view.ParamStruct = new ParamStruct(2, _defaultName);

        Assert.AreNotEqual(view.ParamStruct.Id, model.ParamStruct.Id);
    }

    [Test]
    public void CompareStringTest1()
    {
        //インスタンスを渡している
        view.ParamStruct = model.ParamStruct;

        view.ParamStruct = new ParamStruct(_defaultId, "HHHH");

        Assert.AreEqual(view.ParamStruct.Name, model.ParamStruct.Name);
    }

    [Test]
    public void CompareStringTest2()
    {
        var newParam = new ParamStruct(model.ParamStruct);
        view.ParamStruct = newParam;

        view.ParamStruct = new ParamStruct(_defaultId, "HHHH");
        Assert.AreNotEqual(view.ParamStruct.Name, model.ParamStruct.Name);
    }

    [Test]
    public void ComparewithShallowCopyTest1()
    {
        //インスタンスを渡している
        view.ParamStruct = model.ParamStruct;

        view.ParamStruct = new ParamStruct(2,"HHHH");

        Assert.AreEqual(view.ParamStruct.Id, model.ParamStruct.Id);
        Assert.AreEqual(view.ParamStruct.Name, model.ParamStruct.Name);
    }

    [Test]
    public void ComparewithShallowCopyTest2()
    {
        var newParam = new ParamStruct(model.ParamStruct);
        view.ParamStruct = newParam;

        view.ParamStruct = new ParamStruct(2, "HHHH");

        Assert.AreNotEqual(view.ParamStruct.Id, model.ParamStruct.Id);
        Assert.AreNotEqual(view.ParamStruct.Name, model.ParamStruct.Name);
    }
}
public class TestBase
{
    protected readonly int _defaultId = 1;
    protected readonly string _defaultName = "Hoge";

    public ModelClass model { get; set; }
    public ViewClass view { get; set; }

    [SetUp]
    public void General()
    {
        Debug.Log("GG");
    }

}