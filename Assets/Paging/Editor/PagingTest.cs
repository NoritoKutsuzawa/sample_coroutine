﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.Events;

public class PagingTest
{
    public List<int> CachedDatas { get; set; }
    public int RecordCountPerPage { get; set; } = 5;

    public int CachePageCount { get; set; } = 5;

    public int BasePageNumber { get; set; } = 50;

    [Category("Paging")]
    [TestCase(49, ExpectedResult = false)]
    [TestCase(50, ExpectedResult = true)]
    [TestCase(51, ExpectedResult = true)]
    [TestCase(52, ExpectedResult = true)]
    [TestCase(53, ExpectedResult = true)]
    [TestCase(54, ExpectedResult = true)]
    [TestCase(55, ExpectedResult = false)]
    public bool PageExistTest(int page)
    {
        return PageExist(page);
    }

    private bool PageExist(int page)
    {
        if (page < BasePageNumber) return false;
        if (page > BasePageNumber + CachePageCount - 1) return false;
        return true;
    }

    [Category("Paging")]
    [Test]
    public void ExtractDataTest()
    {
        GenericRequest(1, (data) => { Debug.Log(data); });
        GenericRequest("AAA", Debug.Log);
        GenericRequest(hogeEnum.a, (data) => { Debug.Log(data); });


    }

    private List<int> ExtractData()
    {
        var data = Enumerable.Range(0, 100).OrderByDescending(x => x);
        return new List<int>();
    }

    [SetUp]
    public void Initialize()
    {
        var datas = Enumerable.Range(0, RecordCountPerPage * CachePageCount);
        foreach (var data in datas.OrderByDescending(x => x)) Debug.Log(data);
        CachedDatas = datas.ToList();
        //CachedDatas.ForEach(x => Debug.Log(x));
    }

    private void GenericRequest<T>(T arg, UnityAction<T> callback)
    {
        var data = arg ;
        callback.Invoke(data);
    }
    enum hogeEnum
    {
        a,b,c
    }
}